﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClasesAbstractas
{
	class EmpleadoHonorarios : Empleado
	{
		public decimal SueldoBase { get; set; }

		public override decimal Salario
		{
			get
			{
				return SueldoBase * 0.84m;
			}
		}

		public EmpleadoHonorarios(string nombre, string puesto, decimal sueldobase)
			: base(nombre, puesto)
		{
			SueldoBase = sueldobase;
		}

		public override void Trabajar()
		{
			Console.WriteLine("Trabajando por honorarios...");
		}
	}
}
