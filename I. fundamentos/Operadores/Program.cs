﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace operadores
{
	class Program
	{
		static void Main(string[] args)
		{
			int A = 19;
			int B = 5;
			//Operadores Aritmeticos o Binarios
			Console.WriteLine("Suma: {0}", A + B);
			Console.WriteLine("Resta: {0}", A - B);
			Console.WriteLine("Multiplicacion: {0}", A * B);
			Console.WriteLine("Division: {0}", A / B);
			Console.WriteLine("Modulo: {0}", A % B);

			//Operadores Unarios
			A++; //A+= 1;
			A--; //A-= 1;

			//Operadores de comparación
			Console.WriteLine("Mayor: {0}", A > B);
			Console.WriteLine("Mayor igual: {0}", A >= B);
			Console.WriteLine("Menor: {0}", A < B);
			Console.WriteLine("Menor igual: {0}", A <= B);
			Console.WriteLine("Igual: {0}", A == B);
			Console.WriteLine("Diferente: {0}", A != B);
		}
	}
}
