﻿/*
RETO 1
Hacer un programa que pida al usuario un digito, dicho 
digito va a ser la tabla de multiplicar que va a ser
mostrada en consola
*/
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace tablaMultiplicar
{
	class Program
	{
		static void Main(string[] args)
		{
			for(int b=0; b <= 6; b++)
			{
				Console.WriteLine(b * 2);
			}
			Console.WriteLine("Ingrese un numero del 1 al 10");
			Console.Write("Numero: ");
			string dato1 = Console.ReadLine();
			int n1 = int.Parse(dato1);
			if (n1 > 0 && n1 < 11){
				for (int i = 0; i <= 10; i++){
					int multi = i * n1;
					Console.WriteLine("{0} * {1} = {2}", n1, i, multi);
				}
			}else{
				Console.WriteLine("No intentes burlar el programa.");

				Console.WriteLine("\nIngrese nuevamente un numero que comprenda entre el 1 y el 10");
				Console.Write("Numero: ");
				string dato2 = Console.ReadLine();
				int n2 = int.Parse(dato2);

				if (n2 > 0 && n2 < 11){
					for (int i = 0; i <= 10; i++){
						int multi = i * n1;
						Console.WriteLine("{0} * {1} = {2}", n1, i, multi);
					}
				}else{
					Console.WriteLine("Lo volviste hacer \nAdios...");
					Console.ReadKey();
					Console.Write("\n");
				}
			}
		}
	}
}
