﻿using constructores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodosEstaticos
{
	class Program
	{
		static void Main(string[] args)
		{
			// Para acceder a los miembros estaticos se usa solo el nombre de la clase
			CuentaBancaria cuenta1 = new CuentaBancaria("001", "Simon", 100);
			CuentaBancaria cuenta2 = new CuentaBancaria("002", "Sandra", 250);

			CuentaBancaria.Banco = "Mi Banco";

			Console.WriteLine(cuenta1);
			Console.WriteLine(cuenta2);

			Console.WriteLine("Cambiando el banco...");
			CuentaBancaria.AsignarBanco("Otro Banco");

			Console.WriteLine(cuenta1);
			Console.WriteLine(cuenta2);

		}
	}
}
