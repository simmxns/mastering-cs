﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clases
{
	class Program
	{
		static void Main(string[] args)
		{
			// crear un objeto de tipo rectangulo
			rectangulo rect = new rectangulo();

			rect.Base = 10;
			rect.Altura = 5;

			int area = rect.CalcularArea();
			Console.WriteLine("rect: {0} x {1} = Area: {2}", rect.Base, rect.Altura, area);

			//Sintaxis de inicialización
			rectangulo rect2 = new rectangulo { Base = 3, Altura = 8 };

			int area2 = rect2.CalcularArea();
			Console.WriteLine("rect2: {0} x {1} = Area: {2}", rect2.Base, rect2.Altura, area2);

		}
	}
}
