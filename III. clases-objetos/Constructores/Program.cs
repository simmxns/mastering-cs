﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace constructores
{
	class Program
	{
		static void Main(string[] args)
		{
			//Constructor 1 argumento
			CuentaBancaria cuenta1 = new CuentaBancaria("001");
			Console.WriteLine("NºCuenta: {0}\nUsuario: {1}\nSaldo: {2}", cuenta1.NoCuenta, cuenta1.Usuario, cuenta1.Saldo);

			//Constructos 2 argumentos
			CuentaBancaria cuenta2 = new CuentaBancaria("002", "Simon");
			Console.WriteLine("\nNºCuenta: {0}\nUsuario: {1}\nSaldo: {2}", cuenta2.NoCuenta, cuenta2.Usuario, cuenta2.Saldo);

			//Constructos 3 argumentos
			CuentaBancaria cuenta3 = new CuentaBancaria("002", "Simon", 10500);
			Console.WriteLine("\nNºCuenta: {0}\nUsuario: {1}\nSaldo: {2}", cuenta3.NoCuenta, cuenta3.Usuario, cuenta3.Saldo);
		}
	}
}
